cmake_minimum_required(VERSION 3.12.4)
project(videoPlay)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

#编译为T113
if(TARGET_CPU STREQUAL "t113")
  add_definitions(-DF113_SYS)
  add_definitions(-DENABLE_VIDEO)
endif()

if(TARGET_CPU STREQUAL "t113")
  add_definitions(-DF113_SYS)

  #T113视频库
  set(T113_VIDEO_LIB  tplayer  stdc++  xplayer vdecoder adecoder subdecoder cdc_base
                      VE MemAdapter cdx_parser cdx_playback cdx_stream cdx_base uapi sbm 
                      aftertreatment scaledown fbm videoengine cdx_common ssl crypto )

else()
  #仿真需要的库
  set(SLD_LIB SDL2 avformat avcodec avutil swscale z)
endif()

#头文件目录
include_directories(.)
include_directories(${PROJECT_SOURCE_DIR}/lvgl)
include_directories(${PROJECT_SOURCE_DIR}/lv_drivers)

#添加T113的运行库目录
if(TARGET_CPU STREQUAL "t113")
  set(T113_SDK_DIR "/home/xinxin/project/TinaV2.0-SDK/tina-t113/out/t113-emmc/staging_dir/target/usr")
  include_directories(${T113_SDK_DIR}/include)
  include_directories(${T113_SDK_DIR}/include/allwinner)
  include_directories(${T113_SDK_DIR}/include/allwinner/include)
  link_directories(${T113_SDK_DIR}/lib)
endif()

#添加LVGL项目
add_subdirectory(lvgl)
add_subdirectory(lv_drivers)


file(GLOB_RECURSE SOURCES "application/*.cpp" "custom/*.cpp" "application/*.c" "custom/*.c")
add_executable(${PROJECT_NAME} main.cpp ${SOURCES})


target_link_libraries(${PROJECT_NAME} 
  lvgl 
  lvgl::drivers

  #公共库
  pthread 
  m 
  rt 
  dl

  ${T113_VIDEO_LIB}
  ${SLD_LIB}
)