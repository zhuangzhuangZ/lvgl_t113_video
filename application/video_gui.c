#include "video_gui.h"
#include "lvgl/lvgl.h"
#include <stdlib.h>
#include <unistd.h>

#define TOP_CONT_HIGHT 35
#define PLAY_WINDOW_BOTTOM_CONT_HEIGHT 80

typedef struct
{
  lv_obj_t *screen;
  lv_obj_t *bottom_cont;
  lv_obj_t *pause_img;
  lv_obj_t *list_img;
  lv_obj_t *prog_slider;
  lv_obj_t *video_label;
  lv_obj_t *time_label;
  lv_obj_t *volume_slider;
} play_window_t;

static void play_window_bottom_pause_btn_click_event_cb(lv_event_t *e);
static void play_window_bottom_list_btn_click_event_cb(lv_event_t *e);
static void video_progress_slider_event_cb(lv_event_t *e);
static void volume_slider_event_cb(lv_event_t *e);
static void black_screen_click_event_handler(lv_event_t *e);
static void set_clearscreen_flag_timer_cb(lv_timer_t *t);
static void clear_screen_timer_cb(lv_timer_t *t);

// gui回调函数集
static gui_opt_t gui_opts;

// LCD分辨率
static uint32_t lcd_w = 480;
static uint32_t lcd_h = 272;

// 播放视频窗口
static play_window_t play_window;

// 播放进度slider是否被按下
static bool video_slider_pressed = false;

// 当前是否清屏
static volatile bool video_screen_is_clean = false;

// 清屏定时器
static lv_timer_t *clear_screen_timer = NULL;
static lv_timer_t *clear_screen_timer2 = NULL;

/**
  *@brief 初始化界面UI
  ×@param opts UI回调函数集合
  */
void video_gui_init(gui_opt_t *opts)
{
  lv_obj_t *act = lv_scr_act();

  if (opts != NULL) // 设置回调函数集合
    memcpy(&gui_opts, opts, sizeof(gui_opt_t));

  // 获取显示设备分辨率
  lcd_w = lv_disp_get_hor_res(NULL);
  lcd_h = lv_disp_get_ver_res(NULL);
}

/**
 *@brief 创建播放窗口
 */
void video_gui_create_play_window(void)
{
  // 创建播放黑幕
  lv_obj_t *black_screen = lv_obj_create(lv_scr_act());
  lv_obj_set_size(black_screen, lcd_w, lcd_h);
  lv_obj_clear_flag(black_screen, LV_OBJ_FLAG_SCROLLABLE);
  lv_obj_set_style_bg_color(black_screen, lv_color_hex(0x000000), LV_STATE_DEFAULT);
  lv_obj_set_style_border_width(black_screen, 0, LV_STATE_DEFAULT);
  lv_obj_set_style_pad_all(black_screen, 0, LV_STATE_DEFAULT);
  lv_obj_set_style_radius(black_screen, 0, LV_STATE_DEFAULT);
  lv_obj_add_flag(black_screen, LV_OBJ_FLAG_CLICKABLE);
  lv_obj_add_event_cb(black_screen, black_screen_click_event_handler, LV_EVENT_SHORT_CLICKED, NULL);
  play_window.screen = black_screen;

  // 创建底部容器
  lv_obj_t *cont = lv_obj_create(black_screen);
  lv_obj_set_size(cont, lcd_w, PLAY_WINDOW_BOTTOM_CONT_HEIGHT);
  lv_obj_align(cont, LV_ALIGN_BOTTOM_MID, 0, 0);
  lv_obj_clear_flag(cont, LV_OBJ_FLAG_SCROLLABLE); // 禁止滚动
  lv_obj_set_style_radius(cont, 0, LV_STATE_DEFAULT);
  lv_obj_set_style_border_width(cont, 0, LV_STATE_DEFAULT); // 无边框
  // lv_obj_set_style_bg_color(cont, lv_color_hex(0x000000), LV_STATE_DEFAULT); // 背景黑色
  lv_obj_set_style_bg_color(cont, lv_color_hex(0xffffffff), LV_STATE_DEFAULT); // 背景黑色
  lv_obj_set_style_bg_opa(cont, LV_OPA_60, LV_STATE_DEFAULT);
  play_window.bottom_cont = cont;

  // 创建底部暂停播放
  do
  {
    const char *icon[] = {LV_SYMBOL_PLAY, LV_SYMBOL_LIST};
    lv_obj_t **imgs[] = {&play_window.pause_img, &play_window.list_img};
    lv_event_cb_t img_cbs[] =
        {
            play_window_bottom_pause_btn_click_event_cb,
            play_window_bottom_list_btn_click_event_cb,
        };

    for (int i = 0; i < sizeof(icon) / sizeof(icon[0]); i++) // 创建5img按钮
    {
      imgs[i][0] = lv_img_create(cont); // img按钮
      lv_obj_set_style_text_font(imgs[i][0], &lv_font_montserrat_32, 0);
      lv_img_set_src(imgs[i][0], icon[i]);
      lv_obj_set_style_img_opa(imgs[i][0], LV_OPA_60, LV_STATE_DEFAULT);    // 松开时透明度
      lv_obj_set_style_img_opa(imgs[i][0], LV_OPA_COVER, LV_STATE_PRESSED); // 按下时透明度
      lv_obj_add_flag(imgs[i][0], LV_OBJ_FLAG_CLICKABLE);
      lv_obj_add_event_cb(imgs[i][0], img_cbs[i], LV_EVENT_SHORT_CLICKED, NULL);

      if (i != 0)
        lv_obj_align_to(imgs[i][0], imgs[i - 1][0], LV_ALIGN_OUT_RIGHT_MID, 10, 0);
      else
        lv_obj_align(imgs[i][0], LV_ALIGN_LEFT_MID, 0, 0);
    }
  } while (0);

  // 创建视频进度滑块条
  lv_obj_t *slider = lv_slider_create(cont);
  lv_obj_set_size(slider, lcd_w - 64 * 3 - 100, 10);

  lv_obj_set_style_bg_color(slider, lv_palette_darken(LV_PALETTE_GREY, 3), LV_STATE_DEFAULT);
  lv_obj_set_style_bg_opa(slider, LV_OPA_60, LV_STATE_DEFAULT);
  lv_obj_set_style_bg_color(slider, lv_palette_darken(LV_PALETTE_GREY, 1), LV_PART_INDICATOR | LV_STATE_DEFAULT);
  lv_obj_set_style_bg_color(slider, lv_palette_darken(LV_PALETTE_GREY, 1), LV_PART_KNOB | LV_STATE_DEFAULT);

  lv_obj_align_to(slider, play_window.list_img, LV_ALIGN_OUT_RIGHT_MID, 15, 0);
  lv_obj_add_event_cb(slider, video_progress_slider_event_cb, LV_EVENT_RELEASED, NULL);
  lv_obj_add_event_cb(slider, video_progress_slider_event_cb, LV_EVENT_PRESSING, NULL);
  lv_obj_add_event_cb(slider, video_progress_slider_event_cb, LV_EVENT_PRESSED, NULL);
  play_window.prog_slider = slider;

  // 创建视频文件名标签
  lv_obj_t *video_name_label = lv_label_create(cont);
  lv_obj_set_style_text_font(video_name_label, &lv_font_montserrat_18, LV_STATE_DEFAULT);
  lv_label_set_text(video_name_label, "No File");
  lv_obj_align_to(video_name_label, slider, LV_ALIGN_OUT_TOP_LEFT, 0, -5);
  play_window.video_label = video_name_label;

  // 创建视频进度时间标签
  lv_obj_t *time_label = lv_label_create(cont);
  lv_obj_set_style_text_font(time_label, &lv_font_montserrat_16, LV_STATE_DEFAULT);
  lv_label_set_text(time_label, "00:00/00:00");
  lv_obj_set_user_data(slider, time_label);
  lv_obj_align_to(time_label, slider, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 5);
  play_window.time_label = time_label;

  // 创建视频音量标签
  lv_obj_t *volume_img = lv_img_create(cont);
  lv_obj_set_style_text_font(volume_img, &lv_font_montserrat_32, 0);
  lv_img_set_src(volume_img, LV_SYMBOL_VOLUME_MAX);
  lv_obj_align_to(volume_img, slider, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

  // 创建视频音量滑块条
  slider = lv_slider_create(cont);
  lv_slider_set_range(slider, 0, 40);
  lv_obj_set_size(slider, 100, 10);
  lv_obj_set_style_bg_color(slider, lv_palette_darken(LV_PALETTE_GREY, 3), LV_STATE_DEFAULT);
  lv_obj_set_style_bg_opa(slider, LV_OPA_60, LV_STATE_DEFAULT);
  lv_obj_set_style_bg_color(slider, lv_palette_darken(LV_PALETTE_GREY, 1), LV_PART_INDICATOR | LV_STATE_DEFAULT);
  lv_obj_set_style_bg_color(slider, lv_palette_darken(LV_PALETTE_GREY, 1), LV_PART_KNOB | LV_STATE_DEFAULT);
  lv_obj_add_event_cb(slider, volume_slider_event_cb, LV_EVENT_RELEASED, NULL);
  lv_obj_add_event_cb(slider, volume_slider_event_cb, LV_EVENT_PRESSING, NULL);
  lv_obj_align_to(slider, volume_img, LV_ALIGN_OUT_RIGHT_MID, 20, 0);
  play_window.volume_slider = slider;

  int volume_cur = 0;

  if (gui_opts.video_get_volume != NULL)
    volume_cur = gui_opts.video_get_volume();

  lv_slider_set_value(slider, volume_cur, LV_ANIM_OFF);
}

/**
 *@brief 设置视频播放进度显示
 */
void video_gui_set_progress(int cur, int total)
{
  if (video_screen_is_clean == true)
    return;

  lv_slider_set_range(play_window.prog_slider, 0, total);
  if (video_slider_pressed != true) // 未按下时设置
    lv_slider_set_value(play_window.prog_slider, cur, LV_ANIM_OFF);

  lv_label_set_text_fmt(play_window.time_label, "%d:%d/%d:%d", cur / 60, cur % 60, total / 60, total % 60);
}

/**
 *@brief 释放播放窗口
 */
void video_gui_del_play_window(void)
{
  lv_obj_del_async(play_window.screen);
  play_window.screen = NULL;
}

void tim_recount(void)
{
  // 创建清屏定时器，清屏
  if (clear_screen_timer == NULL)
  {
    clear_screen_timer = lv_timer_create(clear_screen_timer_cb, 3000, NULL);
    lv_timer_set_repeat_count(clear_screen_timer, 1); // 单次定时器
  }
  else
  {
    lv_timer_reset(clear_screen_timer); // 重置清屏定时器
  }

  if (clear_screen_timer2 != NULL)
  {
    lv_timer_del(clear_screen_timer2);
    clear_screen_timer2 = NULL;
  }
}

/**
 *@brief 播放一个视频
 */
void Play_video_for_file(char *name)
{
  const char *video_name = name;

  video_gui_create_play_window(); // 创建播放窗口
  video_screen_is_clean = false;  // 标记未清屏

  // 创建清屏定时器，清屏
  if (clear_screen_timer == NULL)
  {
    clear_screen_timer = lv_timer_create(clear_screen_timer_cb, 500, NULL);
    lv_timer_set_repeat_count(clear_screen_timer, 1); // 单次定时器
  }
  else
  {
    lv_timer_reset(clear_screen_timer); // 重置清屏定时器
  }

  if (clear_screen_timer2 != NULL)
  {
    lv_timer_del(clear_screen_timer2);
    clear_screen_timer2 = NULL;
  }

  lv_label_set_text(play_window.video_label, video_name);

  if (gui_opts.video_play != NULL)
    gui_opts.video_play(video_name); // 新视频播放
}

/**
 *@brief 视频播放底部暂停播放img按钮点击事件回调函数
 */
static void play_window_bottom_pause_btn_click_event_cb(lv_event_t *e)
{
  lv_obj_t *btn = lv_event_get_target(e);

  bool video_state = false;

  if (gui_opts.video_get_state != NULL)
    video_state = gui_opts.video_get_state();

  video_state = !video_state;

  if (video_state == true)
  {
    if (gui_opts.video_play != NULL)
      gui_opts.video_play(NULL); // 传入NULL表示继续播放，而不是新视屏播放
  }
  else
  {
    if (gui_opts.video_pause != NULL)
      gui_opts.video_pause();
  }

  tim_recount();

  lv_img_set_src(play_window.pause_img, video_state ? LV_SYMBOL_PLAY : LV_SYMBOL_PAUSE);
}

/**
 *@brief 视频播放底部列表img按钮点击事件回调函数
 */
static void play_window_bottom_list_btn_click_event_cb(lv_event_t *e)
{
  tim_recount();

  video_gui_del_play_window(); // 删除播放窗口

  gui_opts.exit_cb(NULL); // 退出
}

/**
 *@brief 视频进度滑块条滑动事件函数
 */
static void video_progress_slider_event_cb(lv_event_t *e)
{
  lv_obj_t *slider = lv_event_get_target(e);
  lv_event_code_t e_code = lv_event_get_code(e);

  if (e_code == LV_EVENT_RELEASED)
  {
    video_slider_pressed = false;
    int cur = lv_slider_get_value(slider);

    if (gui_opts.video_set_cur != NULL)
      gui_opts.video_set_cur(cur);
  }
  else if (e_code == LV_EVENT_PRESSED)
  {
    video_slider_pressed = true;
  }
  else if (e_code == LV_EVENT_PRESSING)
  {
    tim_recount();
  }
}

/**
 *@brief 音量进度滑块条滑动事件函数
 */
static void volume_slider_event_cb(lv_event_t *e)
{
  lv_obj_t *slider = lv_event_get_target(e);
  lv_event_code_t e_code = lv_event_get_code(e);

  if (e_code == LV_EVENT_RELEASED)
  {
    int v = lv_slider_get_value(slider);

    if (gui_opts.video_set_volume != NULL)
      gui_opts.video_set_volume(v);
  }
  else if (e_code == LV_EVENT_PRESSING)
  {
    tim_recount();
  }
}

/**
 *@brief 视频播放窗口点击事件回调函数
 */
static void black_screen_click_event_handler(lv_event_t *e)
{
  // 无效化底层容器
  lv_obj_invalidate(play_window.bottom_cont);
  video_screen_is_clean = false; // 标记未清屏

  tim_recount();
}

static void clear_screen_timer2_cb(lv_timer_t *t)
{
  clear_screen_timer2 = NULL;

  if (gui_opts.video_clear_screen != NULL)
    gui_opts.video_clear_screen();
}

/**
 *@brief  清屏定时器回调函数
 */
static void clear_screen_timer_cb(lv_timer_t *t)
{
  int timeOut = 0;
  clear_screen_timer = NULL;
  video_screen_is_clean = true;

  // 创建清屏定时器，清屏
  if (clear_screen_timer2 == NULL)
  {
    clear_screen_timer2 = lv_timer_create(clear_screen_timer2_cb, 500, NULL);
    lv_timer_set_repeat_count(clear_screen_timer2, 1); // 单次定时器
  }
  else
  {
    lv_timer_reset(clear_screen_timer2); // 重置清屏定时器
  }
}