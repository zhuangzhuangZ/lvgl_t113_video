#include "video.h"
#ifdef ENABLE_VIDEO
#include "player.h"
#endif
#include <unistd.h>
#include <dirent.h>
#include "../custom/sys.h"
#include <iostream>

int thread_func(Video *obj);
void video_clear_screen(void);
bool video_get_state(void);
int video_get_volume(void);
void video_pause(void);
void video_play(const char *name);
void video_set_cur(int cur);
void video_set_volume(int volume);

static MediaPlayer *gMplayer = nullptr;

/**
 *@brief Video构造函数
 *@param exit_cb 退出进程回调函数
 */
Video::Video(exit_cb_t exit_cb, mutex &UIMutex, char *file_name)
{
    thread_exit_flag = false;
    ui_mutex = &UIMutex;
    video_path = file_name;

    // 设置UI回调函数
    gui_opt_t gui_opts = {0};
    gui_opts.exit_cb = exit_cb;
    gui_opts.video_clear_screen = video_clear_screen;
    gui_opts.video_get_state = video_get_state;
    gui_opts.video_get_volume = video_get_volume;
    gui_opts.video_pause = video_pause;
    gui_opts.video_play = video_play;
    gui_opts.video_set_cur = video_set_cur;
    gui_opts.video_set_volume = video_set_volume;

    video_gui_init(&gui_opts);

    pthread = new thread(thread_func, this); // 创建执行线程
}

Video::~Video()
{
    thread_exit_flag = true;
    pthread->join();
    delete pthread;
}

int thread_func(Video *obj)
{
    usleep(50000);

    gMplayer = new MediaPlayer(); // 创建播放器

    obj->getUIMutex()->lock();
    Play_video_for_file(obj->video_path);
    obj->getUIMutex()->unlock();

    while (!obj->getExitFlag())
    {
        int cur = gMplayer->GetCurrent();
        int total = gMplayer->GetDuration();

        obj->getUIMutex()->lock();
        video_gui_set_progress(cur / 1000, total / 1000);
        obj->getUIMutex()->unlock();

        usleep(50000);
    }

    delete gMplayer;

    return 0;
}

/**
 * @brief UI清屏回调函数
 */
void video_clear_screen(void)
{
    sys_display_clear();
}

/**
 * @brief UI获取视频播放状态回调函数
 */
bool video_get_state(void)
{
    bool state = false;

    if (gMplayer != nullptr)
        state = gMplayer->GetState();

    return state;
}

/**
 * @brief UI获取音量回调函数
 */
int video_get_volume(void)
{
    int volume = 0;

    if (gMplayer != nullptr)
        volume = gMplayer->GetVolume();

    return volume;
}

/**
 * @brief UI暂停视频毁掉寒酸
 */
void video_pause(void)
{
    if (gMplayer != nullptr)
        gMplayer->Pause();
}

/**
 * @brief UI播放视频回调函数
 */
void video_play(const char *name)
{
    if (name == NULL)
    {
        gMplayer->Start(); // 继续播放
        return;
    }

    // 播放新的视频
    string url = string(name);
    gMplayer->PlayNewVideo(url);
    gMplayer->Start();
}

/**
 * @brief UI设置播放时间点回调函数
 */
void video_set_cur(int cur)
{
    if (gMplayer != nullptr)
        gMplayer->SetCurrent(cur * 1000);
}

/**
 * @brief UI设置音量回调函数
 */
void video_set_volume(int volume)
{
    if (gMplayer != nullptr)
        gMplayer->SetVolume(volume);
}
