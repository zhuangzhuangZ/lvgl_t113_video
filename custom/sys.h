#ifndef _VIDEO_SYS_H_
#define _VIDEO_SYS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "lvgl/lvgl.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
    void sys_exit(void);
    void sys_display_clear(void);
#ifdef __cplusplus
}
#endif

#endif
