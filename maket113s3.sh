###
 # @Author: XinXin
 # @Date: 2023-10-29 21:34:40
 # @LastEditors: XinXin
 # @LastEditTime: 2023-10-30 01:20:48
 # @FilePath: /maket113s3.sh
 # @Description: 
### 
#wayland-protocols
mkdir build-t113
cd build-t113
export STAGING_DIR=./

cmake .. -DTARGET_CPU="t113" -DCMAKE_C_COMPILER=/home/xinxin/project/TinaV2.0-SDK/tina-t113/prebuilt/gcc/linux-x86/arm/toolchain-sunxi-glibc/toolchain/bin/arm-openwrt-linux-gcc

make #-j8
