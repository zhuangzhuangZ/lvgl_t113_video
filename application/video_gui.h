#ifndef __VIDEO_GUI_H__
#define __VIDEO_GUI_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "lvgl/lvgl.h"

    typedef void (*exit_cb_t)(lv_event_t *);
    typedef bool (*video_get_state_t)(void);
    typedef void (*video_pause_t)(void);
    typedef void (*video_play_t)(const char *);
    typedef void (*video_set_cur_t)(int);
    typedef int (*video_get_volume_t)(void);
    typedef void (*video_set_volume_t)(int);
    typedef void (*video_clear_screen_t)(void);

    typedef struct _gui_opt
    {
        exit_cb_t exit_cb;
        video_get_state_t video_get_state;       // 获取播放状态
        video_pause_t video_pause;               // 当前视频暂停回调函数
        video_play_t video_play;                 // 视频播放回调函数
        video_set_cur_t video_set_cur;           // 视频设置进度回调函数
        video_get_volume_t video_get_volume;     // 获取视频音量回调函数
        video_set_volume_t video_set_volume;     // 设置视频音量回调函数
        video_clear_screen_t video_clear_screen; // 清除屏幕回调函数
    } gui_opt_t;

    void video_gui_init(gui_opt_t *opts);
    void video_gui_create_play_window(void);
    void video_gui_set_progress(int cur, int total);
    void video_gui_del_play_window(void);
    void Play_video_for_file(char *name);

#ifdef __cplusplus
}
#endif

#endif
