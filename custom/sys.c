#include "sys.h"
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lv_drivers/sdl/sdl.h"
#include "lv_drivers/indev/evdev.h"
#include "lv_drivers/display/fbdev.h"

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "lvgl/lvgl.h"
#include "lv_drv_conf.h"

void sys_exit(void)
{
#if USE_SDL
#else
    fbdev_exit();
#endif
}

void sys_display_clear(void)
{
    system("dd if=/dev/zero of=/dev/fb0");
}
